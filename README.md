# lab3 - Anova i kontrasti
>author Miloš Sukara

## Requirments
Latest version of node.

## Project setup
```
npm install
```

## Compiles and hot-reloads for development
```
npm run electron:serve
```

## Compiles and minifies for production
```
npm run electron:build
```

Output is in `dist_electron` folder.
